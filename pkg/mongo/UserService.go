package mongo

import (
	"gopkg.in/mgo.v2"
	"projects/thepoty/pkg/root"
)

type UserService struct {
	collection *mgo.Collection
}

func NewUserService(session *Session, dbname string, collectionName string) *UserService {
	collection := session.GetCollection(dbname, collectionName)
	collection.EnsureIndex(userModelIndex())
	return &UserService{collection: collection}
}

func (*UserService) CreateUser(u *root.User) error {
	panic("implement me")
}

func (*UserService) AuthUser(u *root.User) bool {
	panic("implement me")
}
