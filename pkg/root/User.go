package root

type User struct {
	Id       string
	Username string
	Password string
}

type UserService interface {
	CreateUser(u *User) error
	AuthUser(u *User) bool
}
